use proc_macro2::Span;
use quote::{ToTokens, TokenStreamExt, quote};
use std::collections::HashSet;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum StrOrU32 {
    U32(u32),
    Str(String),
}

/// The index attribute.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Idx {
    /// A regular, non-borrowing index.
    N(StrOrU32),
    /// An index which indicates that the value borrows from the decoding input.
    B(StrOrU32),
}

impl ToTokens for Idx {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        let t = match self.val() {
            StrOrU32::U32(i) => {
                let lit = proc_macro2::Literal::u32_unsuffixed(*i);
                quote!{minicbor::data::Idx::U32( #lit )}
            },
            StrOrU32::Str(i) => {
                let lit = proc_macro2::Literal::string(i);
                quote!{minicbor::data::Idx::Str( #lit )}
            },
        };
        tokens.append_all(t)
    }
}

impl Idx {
    /// Test if `Idx` is the `B` variant.
    pub fn is_b(&self) -> bool {
        matches!(self, Idx::B(_))
    }

    /// Get the index value.
    pub fn val(&self) -> &StrOrU32 {
        match self {
            Idx::N(i) => i,
            Idx::B(i) => i
        }
    }

    /// Construct literal for the underlying primitive value
    pub fn primitive_literal(&self) -> proc_macro2::Literal {
        match self.val() {
            StrOrU32::U32(i) => proc_macro2::Literal::u32_unsuffixed(*i),
            StrOrU32::Str(i) => proc_macro2::Literal::string(i),
        }
    }
}

/// Check that there are no duplicate `Idx` values in `iter`.
pub fn check_uniq<'a, I>(s: Span, iter: I) -> syn::Result<()>
where
    I: IntoIterator<Item = &'a Idx>
{
    let mut set = HashSet::new();
    let mut ctr = 0;
    for u in iter {
        set.insert(u.val());
        ctr += 1;
    }
    if ctr != set.len() {
        return Err(syn::Error::new(s, "duplicate index numbers"))
    }
    Ok(())
}

