use minicbor::{Encode, Decode};

#[derive(Decode, Encode, Debug)]
#[cbor(map)]
enum StrEnum {
    #[n("str")] Str(#[n("str")]u8),
    #[n(0)] N(#[n(0)]u8),
}

#[derive(Decode, Encode, Debug)]
#[cbor(map)]
struct Struct {
    #[n("str")]
    str: u8,
    #[n(0)]
    n: u8,
}
